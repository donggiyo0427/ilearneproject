package com.learningman.ilearne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IlearneApplication {

	public static void main(String[] args) {
		SpringApplication.run(IlearneApplication.class, args);
	}

}
